import unittest
import config as cfg

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class TestTextSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('/Users/drosinsk/Downloads/chromedriver')

    def testSearchText(self):
        driver = self.driver
        driver.get(cfg.drivers_config["URL"])
        self.assertIn("Wikipedia", driver.title)
        search_bar = driver.find_element(By.NAME, "search")
        search_bar.clear()
        search_bar.send_keys("lion king")
        search_bar.send_keys(Keys.RETURN)
        assert "https://en.wikipedia.org/wiki/The_Lion_King" == driver.current_url

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()

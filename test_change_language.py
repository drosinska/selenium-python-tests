import unittest
import config as cfg

from selenium import webdriver
from selenium.webdriver.common.by import By


class TestLanguageChange(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('/Users/drosinsk/Downloads/chromedriver')

    def testLanguageChange(self):
        driver = self.driver
        driver.get(cfg.drivers_config["URL"])
        language_button = driver.find_element(By.ID, "js-link-box-it")
        language_button.click()
        assert "https://it.wikipedia.org/wiki/Pagina_principale" == driver.current_url

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()

import unittest
import config as cfg

from selenium import webdriver
from selenium.webdriver.common.by import By


class TestCorrectLogin(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(r"C:\Users\drosinsk\Downloads\chromedriver_win32\chromedriver")

    def testLoginCorrect(self):
        driver = self.driver
        driver.get(cfg.drivers_config["URL"])
        self.assertIn("Statistical Process Control", driver.title)

        email_txt = driver.find_element(By.ID, "Email")
        email_txt.clear()
        email_txt.send_keys(cfg.drivers_config["user"])

        password_txt = driver.find_element(By.ID, "Password")
        password_txt.clear()
        password_txt.send_keys(cfg.drivers_config["password"])

        login_btn = driver.find_element(By.CSS_SELECTOR, "button[type=submit]")
        login_btn.click()

        welcome_elm = driver.find_element(By.CSS_SELECTOR, ".profile_info>h2")

        assert(welcome_elm.is_displayed())

        if "Welcome" in welcome_elm.text:
            doesWelcomeExists = True

        self.assertTrue(doesWelcomeExists)

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
